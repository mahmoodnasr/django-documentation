# README #

This repository is for using [Sphinx](https://pypi.org/project/Sphinx/) in our projects

### How do I get set up? ###

* Install Sphinx
```
pip install Sphinx
```
* Create a docs directory on your project and cd into this directory
```
mkdir docs
cd docs
```
* Setup Sphinx
```
sphinx-quickstart
```
* Open source/conf.py
* Configure path to root directory
```python
import os
import sys
import django
sys.path.insert(0, os.path.abspath('..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "my_project.settings")
django.setup()
```
* Generate the documentation 
```
make html
```

### Documentation Template ###
##### What do we need to know ##### 
1. Function description
1. Is it used internal or have URL
1. URL if exist
1. What application 
1. Parameter and it is description
1. Function return
    1. the method if exist
    1. return type ( HttpResponse / HttpResponseRedirect / dictionary / function call / ... ) 
    2. return description ( template / redirect / response / ... )

##### Examples #####
* Simple return
```python
def my_function(id):
    """
    my_function description ( summary ) .

    :intern: True
    :application: ``My Application``
    :param id: description this parameter
    :return: my object
    """
```
* Complex return
```python
def my_function(request,id):
    """
    my_function description ( summary ) .

    :intern: False
    :url: my_function
    :application: ``My Application``
    :param request: WSGIRequest
    :param id: description this parameter
    :method:
        - **POST**
            - return: HttpResponse
                - template: *myTemplate.html*
                - redirect: *myURL*
                - dictionary: start with cf_
        - **GET**
            - return: HttpResponse
                - template: *myTemplate.html*
      """
```